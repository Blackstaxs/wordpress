#!/bin/bash

# Update and upgrade system
sudo apt update -y
sudo apt upgrade -y

# Install Apache
sudo apt install apache2 -y

# Install PHP and MySQL
sudo apt install -y vim php libapache2-mod-php php-mysql

sudo apt install -y mysql-server

# Download and install WordPress
cd /tmp
wget https://wordpress.org/latest.tar.gz
tar -xvf latest.tar.gz
sudo mv wordpress/ /var/www/html

# Configure wp-config.php
cd /var/www/html/wordpress
sudo cp wp-config-sample.php wp-config.php
#sudo sed -i 's/database_name_here/wp/' wp-config.php
#sudo sed -i 's/username_here/wp_user/' wp-config.php
#sudo sed -i 's/password_here/123456/' wp-config.php
#sudo sed -i 's/localhost/127.0.0.1:3306/' wp-config.php
sudo sed -i "/<?php/a if (strpos(\$_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') !== false) {\n    \$_SERVER['HTTPS'] = 'on';\n}" /var/www/html/wordpress/wp-config.php


cd /etc/apache2/sites-available/
sudo sed -i "s|DocumentRoot /var/www/html|DocumentRoot "/var/www/html/wordpress"|" "/etc/apache2/sites-available/000-default.conf"

sudo service apache2 restart
