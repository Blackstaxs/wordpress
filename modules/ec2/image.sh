#!/bin/bash

sudo apt update -y
sudo apt-get upgrade -y

sudo apt-get install -y docker.io
sudo apt-get install -y awscli
sudo apt-get install -y docker-compose
sudo apt-get install -y php7.4-fpm


aws ecr-public get-login-password --region us-east-1 | docker login --username AWS --password-stdin $DOCKER_AUTH
# Pull the Docker image from Docker Hub
sudo docker pull public.ecr.aws/w3w5l3i9/diegoruiz-wp:latest


sudo mkdir -p ~/docker-compose

echo "version: \"3\"

services:
  mysql:
    container_name: mysql
    image: mysql:latest
    volumes:
      - mysql_data:/var/lib/mysql
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: 123456
      MYSQL_DATABASE: wp
      MYSQL_USER: wp_user
      MYSQL_PASSWORD: 123456

  wordpress:
    container_name: wordpress
    depends_on:
      - mysql
    image: public.ecr.aws/w3w5l3i9/diegoruiz-wp:latest
    ports:
      - "80:80"
    restart: always

volumes:
  mysql_data: {}" > docker-compose.yml



# Run docker-compose
sudo docker-compose up -d

echo done
