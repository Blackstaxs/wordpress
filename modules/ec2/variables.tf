variable "name_prefix" {}

variable "devops_tag" {}

variable "project_tag" {}

variable "env_tag" {}

variable "ami_id" {}

variable "instance_type" {}

variable "key_pair" {}

variable "domain_name" {}

variable "route53_zone_id" {}

variable "acm_certificate_arn" {}