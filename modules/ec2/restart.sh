#!/bin/bash

PHP_VERSION=$(php -r "echo PHP_MAJOR_VERSION.'.'.PHP_MINOR_VERSION;")

cd /var/www/html/wordpress

sed -i "s/database_name_here/wp/" wp-config.php

sed -i "s/username_here/wp_user/" wp-config.php

sed -i "s/password_here/123456/" wp-config.php

sed -i "s/localhost/mysql.diegoruiz-namespace/" wp-config.php


sudo service mysql start
sudo service apache2 start
sudo service php${PHP_VERSION}-fpm start

tail -f /dev/null
