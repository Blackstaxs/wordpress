output "subnet1" {
    value = aws_subnet.subnet1.id
}

output "subnet2" {
    value = aws_subnet.subnet2.id
}

output "vpc" {
    value = aws_vpc.vpc.id
}

output "security_group" {
    value = aws_security_group.security_group.id
}

output "target_group" {
  value = aws_lb_target_group.target_group.arn
}

output "alb_listener" {
  value = aws_lb_listener.listener.arn
}