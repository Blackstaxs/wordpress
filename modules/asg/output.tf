output "asg_name" {
  value = aws_autoscaling_group.ecs_asg.id
}

output "asg_arn" {
  value = aws_autoscaling_group.ecs_asg.arn
}