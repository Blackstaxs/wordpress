variable "name_prefix" {}

variable "devops_tag" {}

variable "project_tag" {}

variable "env_tag" {}

variable "ami_id" {}

variable "instance_type" {}

variable "key_pair" {}

variable "security_group" {}

variable "subnet1" {}

variable "subnet2" {}

variable "aws_region" {}