#!/bin/bash

exec > >(tee -a ${LOG_FILE} )
exec 2> >(tee -a ${LOG_FILE} >&2)

#sudo amazon-linux-extras install ecs -y
sudo su
yum update -y
amazon-linux-extras install ecs -y
echo "ECS_CLUSTER=${ECS_CLUSTER}-ecs-cluster" >> /etc/ecs/ecs.config
systemctl enable --now --no-block ecs.service


echo ecs started
