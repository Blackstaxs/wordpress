resource "aws_ecs_cluster" "cluster" {
  name = "${var.name_prefix}-ecs-cluster"
  tags = {
    "Name"        = "${var.name_prefix}-wordpress"
    "DevOps"      = var.devops_tag
    "Project"     = var.project_tag
    "Environment" = var.env_tag
  }
}

resource "aws_ecs_task_definition" "mysql" {
  family                   = "${var.name_prefix}-mysql"
  network_mode             = "awsvpc"
  cpu                      = "512"
  memory                   = "768"
  task_role_arn            = aws_iam_role.ecs_task_execution_role.arn
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn

  container_definitions = jsonencode([
    {
        name  = "mysql"
        image = "mysql:5.7"
        portMappings = [{
            containerPort = 3306,
            hostPort      = 3306,
        }]
        environment = [
            { name = "MYSQL_ROOT_PASSWORD", value = aws_secretsmanager_secret_version.root_password.secret_string },
            { name = "MYSQL_DATABASE", value = "wp" },
            { name = "MYSQL_USER", value = "wp_user" },
            { name = "MYSQL_PASSWORD", value = aws_secretsmanager_secret_version.db_password.secret_string },
            { name = "SERVICE_NAME", value = "mysql" },
        ]
        mountPoints = [{
            containerPath = "/var/lib/mysql",
            sourceVolume  = "mysql-data",
        }]
        essential = true
    }
  ])

  volume {
    name = "mysql-data"
  }

  tags = {
    "Name"        = "${var.name_prefix}-wp"
    "DevOps"      = var.devops_tag
    "Project"     = var.project_tag
    "Environment" = var.env_tag
  }
}

resource "aws_ecs_task_definition" "wordpress" {
  family                   = "${var.name_prefix}-wp"
  network_mode             = "awsvpc"
  cpu                      = "512"
  memory                   = "768"
  task_role_arn            = aws_iam_role.ecs_task_execution_role.arn
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn

  container_definitions = jsonencode([
    {
        name  = "wordpress"
        image = "public.ecr.aws/w3w5l3i9/diegoruiz-wp:latest"
        portMappings = [{
            containerPort = 80,
            hostPort      = 80,
        }]
        environment = [
            { name = "MYSQL_DATABASE", value = "wp" },
            { name = "MYSQL_USER", value = "wp_user" },
            { name = "MYSQL_PASSWORD", value = aws_secretsmanager_secret_version.db_password.secret_string }, 
            { name = "DB_HOST", value = "${aws_service_discovery_service.mysql_service.name}.${aws_service_discovery_private_dns_namespace.namespace.name}" },         
            { name = "SERVICE_NAME", value = "wordpress" },
        ]
        essential = true
    }
  ])

  tags = {
    "Name"        = "${var.name_prefix}-wp"
    "DevOps"      = var.devops_tag
    "Project"     = var.project_tag
    "Environment" = var.env_tag
  }
}

resource "aws_ecs_service" "mysql" {
  name            = "${var.name_prefix}-mysql"
  cluster         = aws_ecs_cluster.cluster.id
  task_definition = aws_ecs_task_definition.mysql.arn
  desired_count   = 1
  force_new_deployment = true

  network_configuration {
    subnets          = [var.subnet1, var.subnet2]
    security_groups  = [var.security_group]
  }

  capacity_provider_strategy {
    capacity_provider = aws_ecs_capacity_provider.ecs_capacity_provider.name
    weight            = 100
    base              = 1
  }

  service_registries {
    registry_arn = aws_service_discovery_service.mysql_service.arn
  }

  tags = {
    "Name"        = "${var.name_prefix}-wp"
    "DevOps"      = var.devops_tag
    "Project"     = var.project_tag
    "Environment" = var.env_tag
  }

  depends_on = [ var.asg ]
}

resource "aws_ecs_service" "wordpress" {
  name            = "${var.name_prefix}-wp"
  cluster         = aws_ecs_cluster.cluster.id
  task_definition = aws_ecs_task_definition.wordpress.arn
  desired_count   = 1
  force_new_deployment = true

  network_configuration {
    subnets          = [var.subnet1, var.subnet2]
    security_groups  = [var.security_group]
  }

  load_balancer {
    target_group_arn = var.target_group
    container_name   = "wordpress"
    container_port   = 80
  }

  capacity_provider_strategy {
    capacity_provider = aws_ecs_capacity_provider.ecs_capacity_provider.name
    weight            = 100
    base              = 1
  }

  service_registries {
    registry_arn = aws_service_discovery_service.wordpress_service.arn
  }

  tags = {
    "Name"        = "${var.name_prefix}-wp"
    "DevOps"      = var.devops_tag
    "Project"     = var.project_tag
    "Environment" = var.env_tag
  }

  depends_on = [ var.asg ]
}

resource "aws_ecs_capacity_provider" "ecs_capacity_provider" {
 name = "${var.name_prefix}-capacity"

 auto_scaling_group_provider {
   auto_scaling_group_arn = var.asg_arn

   managed_scaling {
     maximum_scaling_step_size = 1000
     minimum_scaling_step_size = 1
     status                    = "ENABLED"
     target_capacity           = 1
   }
 }
}

resource "aws_ecs_cluster_capacity_providers" "cluster_capacity_provider" {
 cluster_name = aws_ecs_cluster.cluster.name

 capacity_providers = [aws_ecs_capacity_provider.ecs_capacity_provider.name]

}

resource "aws_service_discovery_private_dns_namespace" "namespace" {
  name = "${var.name_prefix}-namespace"
  vpc = var.vpc
}

resource "aws_service_discovery_service" "mysql_service" {
  name    = "mysql"
  dns_config {
    namespace_id = aws_service_discovery_private_dns_namespace.namespace.id
    dns_records {
      ttl  = 10
      type = "A"
    }
  }
}

resource "aws_service_discovery_service" "wordpress_service" {
  name    = "wordpress"
  dns_config {
    namespace_id = aws_service_discovery_private_dns_namespace.namespace.id
    dns_records {
      ttl  = 10
      type = "A"
    }
  }
}

resource "aws_iam_role" "ecs_task_execution_role" {
  name = "Diego-wp-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "ecs-tasks.amazonaws.com",
        },
      },
    ],
  })

  tags = {
    "Name"        = "${var.name_prefix}-wordpress"
    "DevOps"      = var.devops_tag
    "Project"     = var.project_tag
    "Environment" = var.env_tag
  } 
}

resource "aws_iam_role_policy_attachment" "ecs_task_execution_role_policy" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
  role       = aws_iam_role.ecs_task_execution_role.name
}

resource "random_password" "db_password" {
  length           = 16
  special          = true
  override_special = "_%@"
}

resource "random_password" "root_password" {
  length           = 16
  special          = true
  override_special = "_%@"
}

resource "aws_secretsmanager_secret" "db_pw" {
  name = "${var.name_prefix}-db-pw"
  recovery_window_in_days = 0
  tags = {
    "Name"        = "${var.name_prefix}-p"
    "DevOps"      = var.devops_tag
    "Project"     = var.project_tag
    "Environment" = var.env_tag
  } 
}

resource "aws_secretsmanager_secret_version" "db_password" {
  secret_id     = aws_secretsmanager_secret.db_pw.id
  secret_string = random_password.db_password.result
}

resource "aws_secretsmanager_secret_version" "root_password" {
  secret_id     = aws_secretsmanager_secret.db_pw.id
  secret_string = random_password.root_password.result
}

