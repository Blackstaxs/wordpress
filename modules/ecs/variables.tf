variable "name_prefix" {}

variable "devops_tag" {}

variable "project_tag" {}

variable "env_tag" {}

variable "subnet1" {}

variable "subnet2" {}

variable "security_group" {}

variable "target_group" {}

variable "alb_listener" {}

variable "asg_arn" {}

variable "asg" {}

variable "vpc" {}

variable "db_name" {}

variable "db_user" {}
