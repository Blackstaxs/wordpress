FROM ubuntu:20.04

# Set non-interactive environment
ENV DEBIAN_FRONTEND=noninteractive

# Install necessary packages
RUN apt-get update && \
    apt-get install -y wget tar apache2 mysql-server

# Copy your script
COPY modules/ec2/script.sh /usr/local/bin/script.sh

COPY modules/ec2/restart.sh /usr/local/bin/restart.sh

RUN apt-get update && apt-get install -y sudo

# Set the timezone to UTC
RUN ln -fs /usr/share/zoneinfo/UTC /etc/localtime

# Create the document root directory
RUN mkdir -p /var/www/html/wordpress

# Run your script
RUN chmod +x /usr/local/bin/script.sh && \
    /usr/local/bin/script.sh && \
    chmod +x /usr/local/bin/restart.sh

EXPOSE 80

CMD ["/usr/local/bin/restart.sh"]

