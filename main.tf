provider "aws" {
  region     = "${var.aws_region}"
  //profile = "Acklen"
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
  backend "s3" {
    region = "us-east-1"
  }
}


module "ec2" {
  source          = "./modules/ec2"
  name_prefix     = var.name_prefix
  devops_tag      = var.devops_tag
  project_tag     = var.project_tag
  env_tag         = var.env_tag
  ami_id          = var.ami_id
  instance_type   = var.instance_type
  key_pair        = var.key_pair
  route53_zone_id = var.route53_zone_id
  domain_name     = var.domain_name
  acm_certificate_arn = var.acm_certificate_arn
}

module "ecs" {
  source = "./modules/ecs"
  name_prefix     = var.name_prefix
  devops_tag      = var.devops_tag
  project_tag     = var.project_tag
  env_tag         = var.env_tag

  subnet1 = module.ec2.subnet1
  subnet2 = module.ec2.subnet2
  security_group = module.ec2.security_group

  target_group = module.ec2.target_group
  alb_listener = module.ec2.alb_listener
  asg_arn = module.asg.asg_arn
  asg = module.asg
  vpc = module.ec2.vpc
  db_name = var.db_name
  db_user = var.db_user
}


module "asg" {
  source           = "./modules/asg"
  name_prefix      = var.name_prefix
  devops_tag       = var.devops_tag
  project_tag      = var.project_tag
  env_tag          = var.env_tag
  ami_id           = var.ami_id
  instance_type    = var.instance_type
  key_pair         = var.key_pair

  security_group   = module.ec2.security_group
  subnet1 = module.ec2.subnet1
  subnet2 = module.ec2.subnet2
  
  aws_region = var.aws_region
}

